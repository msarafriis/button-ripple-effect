# Button Ripple Effect

Makes buttons look cool.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
